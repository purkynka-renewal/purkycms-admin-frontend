let updateInterval = 5000;

module.exports = function System(){
  const systemapi = $("main#main").data("systemapi");
  const socket = require("socket.io-client")(systemapi, {...global.ioOptions});

  socket.on("connect", refresh);

  $("#reloadConfig").click(function(){
    socket.emit("reloadConfig", ()=>{
      global.GUI.success($(this).data("success"));
    });
  });

  function refresh(){
    // Thread info
    socket.emit("getThreadInfo", (data)=>{
      // Threads icon
      const statusWarn = data.threads == data.maxThreads;
      $("#threadstatus .status-icon")
        .toggleClass("green", !statusWarn)
        .toggleClass("orange", statusWarn);
      $("#threadstatus .status-icon i").removeClass("fa-spinner fa-pulse")
        .addClass("fa-circle");
      // Min threads
      if ($("#threadstatus .minThreads").text() != data.minThreads) {
        $("#threadstatus .minThreads").text(data.minThreads);
      }
      // Active threads
      if ($("#threadstatus .activeThreads").text() != data.threads) {
        $("#threadstatus .activeThreads").text(data.threads);
      }
      // Max threads
      if ($("#threadstatus .maxThreads").text() != data.maxThreads) {
        $("#threadstatus .maxThreads").text(data.maxThreads);
      }
      // Thread fails
      if ($("#threadstatus .threadFails").text() != data.threadFails) {
        // Thread fails icon
        const failWarn = !!data.threadFails;
        $("#threadstatus .fail-icon")
          .toggleClass("green", !failWarn)
          .toggleClass("orange", failWarn);
        $("#threadstatus .fail-icon i").removeClass("fa-spinner fa-pulse")
          .toggleClass("fa-check", !failWarn)
          .toggleClass("fa-exclamation", failWarn);
        // Thread fails text
        $("#threadstatus .threadFails").text(data.threadFails);
      }
    });

    // Usage info
    socket.emit("getUsageInfo", ({cpu, mem})=>{
      $("#sys-cpu").progress({
        precision: 1, autoSuccess: false, showActivity: false, percent: cpu
      });
      $("#sys-mem").progress({
        precision: 1, autoSuccess: false, showActivity: false, percent: mem
      });
    });

    setTimeout(refresh, updateInterval);
  };
}
