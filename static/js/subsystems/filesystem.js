// TODO: https://css-tricks.com/drag-and-drop-file-uploading/
const ioStream = require("socket.io-stream");

module.exports = function filesystem(chooserOptions=false) {
  chooserOptions = chooserOptions ? Object.assign({
    type: "*",
    singular: true,
    onSelectionChange: ()=>{}
  }, chooserOptions) : false;
  const table = this.table = global.dataTableReferences.filesystem;

  const $table = $(table.table().node());
  const $tbody = $(table.table().body());
  const $menu = $("#fs-menu");
  const $bulkmenu = $("#fs-bulkmenu");
  const $dirmenu = $("#fs-dirmenu");
  const $diskusage = $("#fs-diskusage");
  const $breadcrumb = $("#fs-breadcrumb");
  const $breadcrumbItem =
      $($("#fs-breadcrumb-item").prop("content").childNodes).clone();
  const $uploadingItem =  $($("#fs-uploadItem").prop("content").childNodes);
  const $inputFile = $(`<input type="file" multiple>`);

  const apiPath = $menu.data("api");
  const io = require("socket.io-client");
  const socket = this.socket = io(apiPath, {...global.ioOptions});
  const reload = ()=>$(".active", $breadcrumb).click();

  const filesUploading = [];
  this.onSelectionChange =
      chooserOptions ? chooserOptions.onSelectionChange : ()=>{};
  const fs = this;
  let cwd, breadcrumbData;

  //
  // BULK ACTIONS
  //
  // Download files
  $(".download", $bulkmenu).on("click", ()=>{
    downloadFiles(
      getSelectedRows().map(row=>$(row).data("filename")),
      breadcrumbData[breadcrumbData.length-1].origName
    );
  });
  // Delete files
  $(".delete", $bulkmenu).on("click", ()=>{
    deleteFiles(getSelectedRows().map(row=>$(row).data("filename")));
  });

  //
  // FOLDER ACTIONS
  //
  // Download current folder
  $(".download", $dirmenu).on("click", ()=>{
    downloadFiles([""], breadcrumbData[breadcrumbData.length-1].origName);
  });
  // Delete files
  $(".delete", $dirmenu).on("click", ()=>{
    deleteFiles([""]);
  });

  //
  // PLUS ACTIONS
  //
  // Create a folder
  $(".new-folder", $menu).on("click", ()=>{
    const $modal =$($("#fs-newfoldermodal").prop("content").childNodes).clone();
    $modal.appendTo(document.body).modal("show");
    $modal.on("submit", (event)=>{
      event.preventDefault();
      socket.emit("MKDIR", cwd+$("input", $modal).val(), (status, errMsg)=>{
        if (status == 201) reload();
        else GUI.error(errMsg);
      });
      return false;
    });
  });
  // Choose files
  $(".upload-file", $menu).on("click", ()=>{
    $inputFile.click();
  });

  //
  // INTERNAL ACTIONS
  //
  // Upload files
  $inputFile.on("change", ()=>{
    for(const file of $inputFile.prop("files")){
      uploadFile(file);
    }
  });

  // Updated table
  $(document).on("tableUpdate.purky", (_, body)=>{
    $tbody.data("breadcrumb", $(body).data("breadcrumb"));
    updatedContent();
  });

  //
  // FUNCTIONS
  //
  function updatedContent() {
    fs.onSelectionChange(getSelectedRows());

    /// Update breadcrumb
    breadcrumbData = $tbody.data("breadcrumb");
    cwd = "/"+breadcrumbData.slice(1).map(v=>v.origName).join("/")+"/";
    cwd = cwd.replace(/\/+/g,/*/ */ "/");

    $breadcrumb.html("");
    for (let i = 0; i < breadcrumbData.length; i++) {
      const {linkCanonical, name, origName} = breadcrumbData[i];
      const $item = $breadcrumbItem.clone();
      $item.attr("href", linkCanonical);
      if (i == breadcrumbData.length-1) {
        $item.addClass("active");
      }
      $(".title", $item).text(!i ? name : origName);
      $breadcrumb.append($item);
    }

    //
    // Bind row related events
    //
    // Row selection
    $("td:first-child input[type='checkbox']", $table).on("change", function(){
      if ($(this).is(":disabled")) $(this).prop("checked", false);
      const checked = $(this).prop("checked");
      // Force singular selection
      if (checked && chooserOptions && chooserOptions.singular) {
        $("td:first-child input[type='checkbox']", $table).not(this)
            .each(function(){
              $(this).prop("checked", false).change();
            });
      }

      $(this).parents("tr").toggleClass("selected", checked);
      fs.onSelectionChange(getSelectedRows());
    });
    // Select by clicking row
    $(".filename:not([data-order=0]),.thumbnail,.type,.date", $table)
        .on("click", function(event){
      const $row = $(this).parents("tr");
      const input = $("td:first-child input[type='checkbox']", $row);
      input.prop("checked", !input.prop("checked")).change();
    });
    // Delete a file
    $(".delete", $table).on("click", function() {
      const filename = $(this).parents("tr").data("filename");
      deleteFiles([filename]);
    });
    // Download a file
    $(".download", $table).on("click", function() {
      const row = $(this).parents("tr");
      const filename = row.data("filename");
      const type = row.data("type") + "/" + filename.split(".").pop();
      downloadFiles([filename], filename, type);
    });

    /// Set chooser mode
    if (chooserOptions) {
      $("a[data-table='filesystem']").attr("data-pushState", "false");

      if (chooserOptions.type != "*") {
        // Disable unselectable rows
        $("td:first-child input[type='checkbox']", $table).each(function() {
          const $row = $(this).parents("tr");
          if (!chooserOptions.type.includes($row.data("type"))) {
            $(this).prop("disabled", true);
          }
        });

        // Filter the table
        table.columns(3).search("^"+["folder",...chooserOptions.type.split(",")]
            .map(v=>"(?:"+v+")").join("|")+"$", true).draw();
      }
    }
  }

  // Get selected rows
  function getSelectedRows() {
    return $("tr.selected", $tbody).toArray();
  }
  this.getSelectedRows = getSelectedRows;

  // Get selected filenames
  function getSelectedNames() {
    return getSelectedRows().map(row=>({
      name: $(row).data("filename"),
      file: cwd+$(row).data("filename"),
    }));
  }
  this.getSelectedNames = getSelectedNames;

  // Upload a file
  function uploadFile(file) {
    const path = (cwd+file.name).replace(/\/+/g,/*/ */ "/");
    if (filesUploading.includes(path)) return;

    const stream = ioStream.createStream();
    let size = 0;
    filesUploading.push(path);

    const _$uploadingItem = $uploadingItem.clone();
    $(".filename", _$uploadingItem).text(file.name);
    const $progress = $(".uploadProgress", _$uploadingItem);
    table.rows.add(_$uploadingItem);
    table.draw();

    ioStream(socket).emit("PUT", stream, path, (status, errMsg)=>{
      if (status != 201) return GUI.error(errMsg);
      filesUploading.splice(filesUploading.indexOf(path), 1);

      // All files finished uploading
      if(!filesUploading.length){
        reload();
      }
    });
    ioStream.createBlobReadStream(file).on("data", (chunk)=>{
      // Show upload progress
      size += chunk.length;
      const progress = Math.floor(size / file.size * 100 + 0.5);
      $progress.progress({progress});
    }).on("end", ()=>{
      // Uploading finished, waiting for response...
      $progress.addClass("indeterminate");
    }).pipe(stream);
  }

  // Download files
  function downloadFiles(files, name, type) {
    const stream = ioStream.createStream();
    const chunks = [];

    ioStream(socket).emit("GET", stream, {paths: files, cwd},
        (status, isArchive, errMsg)=>{
      if (status != 200) {
        GUI.error(errMsg);
        stream.destroy();
      } else {
        stream.on("data", chunk=>chunks.push(chunk));
        stream.on("end", ()=>{
          if(!chunks.length) return;
          if (isArchive) {
            type = "application/zip";
            name += ".zip";
          }
          const blob = new Blob(chunks, {type});
          const bloburl = URL.createObjectURL(blob);
          $("<a>").attr("download", name).attr("href", bloburl).get(0).click();
          setTimeout(()=>URL.revokeObjectURL(bloburl));
        });
      }
    });
  }

  // Delete files
  function deleteFiles(files) {
    const $modal = $($("#fs-deletemodal").prop("content").childNodes).clone();
    $(".count", $modal).text(files.length);
    $modal.appendTo(document.body).modal({
        onApprove(){
          for (const file of files) {
            socket.emit("DELETE", cwd+file, ()=>{
              files.splice(files.indexOf(file), 1);
              if (!files.length) reload();
            });
          }
        }
      }).modal("show");
  }

  //
  // INIT
  //
  updatedContent();

  // Get disk usage on load
  // $.get(apiPath+"/_diskusage").then((diskUsage)=>{
  //   $diskusage.progress({percent: diskUsage, showActivity: false});
  // });
  socket.emit("getDiskUsage", (diskUsage)=>{
    $diskusage.progress({percent: diskUsage, showActivity: false});
  });

  return this;
}.bind({});
