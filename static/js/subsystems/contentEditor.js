const EasyMDE = require("easymde");

module.exports = function contentEditor(target="form#contentEditor"){
  let $target = $(target);
  let textEditors = {};
  let statusTimeout;

  //
  // Image fields
  //
  $($target)
    // Choose file
    .on("click", ".field-image .choose,.field-image .placeholder", {}, function(){
      fileChooser("image,video").then((result)=>{
        const $parent = $(this).parents(".field-image");
        const $label = $("input[type='text']", $parent);
        if (!$label.val()) $label.val(result.name).change();
        $("input[type='hidden']", $parent).val(result.file).change();
      }).catch(()=>{});
      return false;
    })
    // Remove
    .on("click", ".field-image .remove", {}, function(){
      $(this).parents(".field-image").find("input").val("").change();
      return false;
    })
    // File update
    .on("change", ".field-image input[type='hidden']", {}, function(){
      const $preview = $(".image img", $(this).parents(".field-image"));
      let src = "#";
      if ($(this).val()) src = global.filepath+$(this).val();
      $preview.attr("src", src);
    });

  //
  // Gallery fields
  //
  // Add or remove fields according to inputs
  $(".field-gallery", $target).on("change", "input", {}, function(){
    const $gallery = $(this).parents(".field-gallery");
    let isFull = true;
    const fields = $gallery.children(".field-image");
    let fieldNum = fields.length;
    fields.each(function(){
      const isEmpty = $("input", this).toArray().every((el)=>!$(el).val());
      if (fieldNum > 1 && isEmpty) {
        $(this).remove();
        fieldNum--;
      } else if (isFull){
        isFull = $("input", this).toArray().every((el)=>$(el).val());
      }
    });

    if (isFull) {
      const $item = $($($gallery.data("template")).prop('content').childNodes);
      $gallery.append($item.clone());
    }
  }).find("input").trigger("change");

  //
  // Repetable fields
  //
  $(".field-repeatable", $target)
    // Add new item
    .on("click", ".add", {}, function(){
      $($(this).data("target")).append(
          $($($(this).data("template")).prop('content').childNodes).clone());
      return false;
    })
    // Remove item
    .on("click", ".remove", {}, function(){
      $(this).parent().remove();
      return false;
    });

  //
  // Dynamic datalists/selects
  //
  $("[data-list]", $target).each(function() {
    const $this = $(this);
    const $itemTemplate = $("template", $this).length
        ? $($("template", $this).prop("content").childNodes)
        : $("<option>");

    $.get($this.data("list"), {}, (data)=>{
      data.result.forEach((item)=>{
        $this.append($itemTemplate.clone().text(item));
      });

      // Initiate semantic if needed
      const $ddparent = $this.parents(".dropdown");
      if ($ddparent.length) {
        $ddparent.dropdown({
          allowAdditions: $ddparent.is(".addable")
        });
      }
    }, "json");
  });


  //
  // Text editors
  //
  $("textarea.texteditor", $target).each(function() {
    textEditors[$(this).attr("name")] = new EasyMDE({
      inputStyle: "contenteditable",
      element: this,
      initialValue: $(this).val(),
      forceSync: true,
      theme: "pastel-on-dark",
      autoDownloadFontAwesome: false,
      spellChecker: false,
      hideIcons: ["image", "link", "fullscreen"],
      nativeSpellcheck: true,
      styleSelectedText: true,
      sideBySideFullscreen: false,
      placeholder: $(this).attr("data-placeholder"),
      renderingConfig: {
        codeSyntaxHighlighting: true
      },
    });
  });


  //
  // ONSUBMIT
  //
  $target.on('submit', function () {
    updateShareButtons();

    const data = {};
    const inputs = $target.serializeArray();
    const namedArrayItems = {};
    $target.addClass("loading");

    // Add unchecked checkboxes
    $("input[name][type='checkbox']:not(:checked)").each(function(){
      inputs.push({name:$(this).attr("name"), value:"off"});
    });

    // Parse the data
    for (let {name,value} of inputs) {
      const $source = $("[name='"+name+"']");
      const subnames = name.split(".");
      const finname = subnames.pop();
      let datatarget = data;

      for(const subname of subnames){
        // Array
        if(subname.endsWith("]")) {
          // It exists
          if(subname.slice(0, -6) in datatarget){
            datatarget = datatarget[subname.slice(0, -6)];
          }
          // Create new
          else{
            datatarget = datatarget[subname.slice(0, -6)] = [];
          }
          // The item exists
          const itemname = subname.slice(-5, -1);
          if (itemname in namedArrayItems) {
            datatarget =
                datatarget[datatarget.indexOf(namedArrayItems[itemname])];
          } else {
            datatarget = namedArrayItems[itemname] =
                datatarget[datatarget.push({}) - 1];
          }
        }
        // Object
        else {
          // It exists
          if(subname in datatarget){
            datatarget = datatarget[subname];
          }
          // Create new
          else{
            datatarget = datatarget[subname] = {};
          }
        }
      }

      // Parse value
      switch ($source.attr("type")) {
        case "checkbox":
          value = value == "on";
          break;

        default:
          if ($source.parents(".multiple").length) {
            value = JSON.parse("["+value+"]");
          }
      }
      if (value == "") continue;
      datatarget[finname] = value;
    }

    //
    // Clean-up data
    //


    //
    // SUBMIT
    //
    $.ajax({
      method: $target.attr("method"),
      url: $target.attr("action"),
      data: JSON.stringify(data),
      contentType: "application/json; charset=UTF-8",
      dataType: "json",
      xhrFields: { withCredentials: true },
    }).always(()=>{
      // if(statusTimeout) clearTimeout(statusTimeout);
      // $target.removeClass("loading");
    }).done((data)=>{
      // $target.addClass("success");
      // statusTimeout = setTimeout(()=>{
      //   $target.removeClass("success");
      // }, 10*1000);

      const href = (window.location.pathname+"/").replace(/\/+$/,"").split("/");
      href[href.length-1] = data.result.slug;
      global.PJAX.loadURL(href.join("/"), null, null,
          $(`<a></a>`).data("pjax-anim0", "none").data("pjax-anim1", "none"));
    });
      // always: ()=>$("#contentEditor-saving").hide(),
      // success: (data)=>{
      //   const href = (window.location.href+"/").replace(/\/$/, "").split("/");
      //   const origHref = href.slice().join("/");
      //   href[href.length-1] = data.result.slug;
      //   if(origHref != href.join("/")) global.PJAX.loadURL(href.join("/"));
      //   $("#contentEditor-saved").show().delay(15000).hide();
      // },
      // fail: (result)=>{
      //   // FIXME: this does nothing
      //   $(".error-text", "#contentEditor-error").text(result.error);
      //   $("#contentEditor-error").show().delay(15000).hide();
      // }
    // });
    return false;
  });

  updateShareButtons();

  function updateShareButtons(){
    const prefix = $target.data("text-new");
    const title = encodeURIComponent(prefix+$("[name='title']", $target).val());

    $("#shareOnTwitter").attr("href", "https://twitter.com/intent/tweet?"+
        "text="+title+"&"+
        "url="+"");

    $("#shareOnFacebook").attr("href",
        "https://www.facebook.com/sharer/sharer.php?"+
        "t="+title+"&"+
        "u="+"");
  }
};
