require('../css/style.forefront.scss');
require('codemirror/lib/codemirror.css');
require('codemirror/theme/pastel-on-dark.css');
require('easymde/src/css/easymde.css');
require('../css/libs-fontawesome.scss');
require('../css/libs-semantic.scss');
require('../css/overrides.scss');
require('../css/style.scss');

// Asynchronously add main CSS
const mainStyle = document.createElement("link");
mainStyle.rel = "stylesheet";
mainStyle.href = document.getElementById("mainStyle").href;
const mainStylePromise = new Promise((resolve)=>mainStyle.onload = resolve);
document.head.appendChild(mainStyle);

// Settings
global.ioOptions = {
  path: "/",
  transports: ['websocket', 'polling'],
};

const filesize = require('filesize');
global.jQuery = global.$ = require('jquery');
require('./libs/semantic.min.js');
global.PJAX = require('./PJAX.js');

//
// Placeholder functions
//
global.fileChooser = async (...args)=>
    (import('./modules/fileChooser.js').then(a=>a.default(...args)));


//
// SemanticUI settings
//
$.fn.toast.settings.icons = {
  info    : 'fas fa-info',
  success : 'fas fa-checkmark',
  warning : 'fas fa-warning',
  error   : 'fas fa-times'
};
$.fn.toast.settings.showIcon = true;
$.fn.toast.settings.showProgress = "bottom";
$.fn.toast.settings.displayTime = "auto";

//
// Internals
//
global.dataTableReferences = {};
const isMobileMQ = window.matchMedia("(max-width: 720px)");

//
// Global GUI utility functions
//
global.GUI = {
  error(title, message){
    $("body").toast({class: 'error', message, title });
  },
  success(title, message){
    $("body").toast({class: 'success', message, title });
  }
}

//
// Event handlers
//
// Page content changed
$(document).on("ready.purky", async ()=>{
  mainStylePromise.then(()=>$("body").addClass("ready"));

  // Set global config
  updatePathConfig();

  // Reinit
  global.dataTableReferences = {};

  // Register service worker if possible
  // if(global.basepath && 'serviceWorker' in navigator) {
  //   navigator.serviceWorker.register(global.basepath+'serviceWorker.js');
  //
  //   // navigator.serviceWorker.ready.then(()=>{
  //   //   $("#offlineInfo").show();
  //   // });
  // }

  // Initiate sidebar if not already
  if(!$('#sidebarMenu').is(".push")){
    const isMobile = isMobileMQ.matches;
    $('#sidebarMenu')
      .sidebar('setting', 'transition', isMobile ? 'auto' : 'push')
      .sidebar('setting', 'dimPage', isMobile)
      .sidebar('setting', 'closable', isMobile);
    $('#menuToggler').on('click', ()=>$('#sidebarMenu').sidebar('toggle'));
    if (isMobile) {
      $('#sidebarMenu').addClass("mobile").sidebar("hide");
    }
  }

  //
  // Submodules
  //

  // Initiate datatables if needed
  if($(".datatable").length){
    for (const table of $(".datatable")) {
      await initDatatable($(table));
    }
  }

  // Content editor
  if($("#contentEditor").length) {
    import('./subsystems/contentEditor.js').then(a=>a.default());
  }
  // Content listing
  if($("#contentListing").length) {
    import('./subsystems/contentListing.js').then(a=>a.default());
  }
  // Filesystem
  if($("#filesystem").length) {
    import('./subsystems/filesystem.js').then(a=>a.default());
  }
  // Monitoring
  if($("#threadstatus").length) {
    import('./subsystems/monitoring.js').then(a=>a.default());
  }

  // Trigger tweaks
  contentUpdate();
})
// Table content changed
.on("tableUpdate.purky", contentUpdate);


//
// Functions
//
function contentUpdate(){
  // Transfrom filesizes do humanreadble ones
  $(".filesize").each(function(){
    $(this).text(filesize($(this).text()));
  });
  // Transfrom SQL dates to local ones
  $(".date").each(function(){
    const date = new Date($(this).text().trim());
    if (!isNaN(date.valueOf())) $(this).text(date.toLocaleString());
  });

  // Initiate semantic dropdowns (ignores dynamic and already dropdowns)
  $(".ui.dropdown:not([data-list]):not([dropdown])").each(function(){
    $(this).data("dropdown", true).dropdown({
      allowAdditions: $(this).is(".addable")
    });
  });

  // Tooltips
  $(".ui.tooltip").popup();

  // Copy To Clipboard
  $("[data-c2c]").off("click").on("click", function() {
    navigator.clipboard.writeText($(this).data("c2c")).then(()=>{
      if($(this).is("[data-result-title]")){
        $(this).popup({
          on: "manual", variation: "green", html: $(this).data("result-title"),
          onHide(){ $(this).popup("remove"); }
        }).popup("show");
      }
    });
  });
}
global.contentUpdate = contentUpdate;

//
// Functions
//
async function initDatatable($table){
  await import('datatables.net-se');
  $.fn.DataTable.ext.errMode = 'none';

  const table = $table.DataTable({
    autoWidth: false,
    pageLength: 14,
    drawCallback: function() {
      const table = this;
      // Select all rows checkbox
      $("th:nth-child(1) input[type='checkbox']", this).off("change")
          .on("change", function() {
        $("tr td:nth-child(1) input[type='checkbox']", table)
            .prop("checked", $(this).prop("checked")).change();
      });
    }
  });

  if ($table.data("searchinput")) {
    $($table.data("searchinput")).on("input", function(){
      table.search($(this).val()).draw();
    });
  }

  if($table.is("[id]")){
    global.dataTableReferences[$table.attr("id")] = table;
  }
}
global.initDatatable = initDatatable;

function updatePathConfig(){
  const $main = $("main#main");
  __webpack_public_path__ = $main.data("assetpath");
  global.ioOptions.path = $main.data("iopath");
  global.filepath = $main.data("filepath");
  global.assetpath = $main.data("assetpath");
  global.basepath = $main.data("basepath");
}
