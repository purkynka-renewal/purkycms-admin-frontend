module.exports = function fileChooser(type="*", multiple=false){
  return new Promise(async (resolve, reject)=>{
    const $chooser = $($("#fileChooser").prop("content").childNodes).clone();
    $chooser.appendTo(document.body);

    contentUpdate();
    await initDatatable($("#filesystem"));
    const fs = (await import("../subsystems/filesystem.js")).default({
      type, singular: !multiple
    });

    $(".ok", $chooser).prop("disabled", true);
    fs.onSelectionChange = (selection)=>{
      $(".ok", $chooser).prop("disabled", !selection.length);
    };
    $("#fs-root").click();

    $chooser.modal({
      autofocus: false,
      onApprove() { resolve(fs.getSelectedNames()[0]) },
      onHide(){ reject(); },
      onHidden(){ $chooser.remove(); fs.socket.disconnect(); }
    }).modal("show")
  });
};
